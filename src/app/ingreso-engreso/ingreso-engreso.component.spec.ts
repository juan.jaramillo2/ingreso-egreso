import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoEngresoComponent } from './ingreso-engreso.component';

describe('IngresoEngresoComponent', () => {
  let component: IngresoEngresoComponent;
  let fixture: ComponentFixture<IngresoEngresoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngresoEngresoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IngresoEngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
