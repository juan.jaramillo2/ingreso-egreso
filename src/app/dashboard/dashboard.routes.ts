import {  Routes } from '@angular/router';
import { DetalleComponent } from '../ingreso-engreso/detalle/detalle.component';
import { EstadisticaComponent } from '../ingreso-engreso/estadistica/estadistica.component';
import { IngresoEngresoComponent } from '../ingreso-engreso/ingreso-engreso.component';

export const dashboardRoutes: Routes = [
    {path: '', component:EstadisticaComponent},
    {path: 'ingreso-egreso', component:IngresoEngresoComponent},
    {path: 'detalle', component:DetalleComponent},
    {path: '**', redirectTo : ''},
];

